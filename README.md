# SNMP MIBs

This is a collection of SNMP MIBs.

The `all` directory is intended to be a parse error clean path that can be used with the [snmp_exporter generator](https://github.com/prometheus/snmp_exporter/tree/master/generator).

```
git clone https://gitlab.com/superq/mibs.git
export MIBDIRS=mibs/all
```
